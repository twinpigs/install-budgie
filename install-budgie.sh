dnf group install base-x -y
dnf install lightdm-gtk slick-greeter -y
systemctl set-default graphical.target
dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm -y
dnf install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm -y
dnf copr enable rodrigofarias77/budgie-desktop -y
dnf copr enable mizuo/plata-theme -y
dnf copr enable harryc/papirus-icon-theme -y
#dnf install iwl6000g2b-firmware google-noto-sans-fonts 
dnf install plata-theme papirus-icon-theme breeze-cursor-theme google-noto-sans-fonts budgie-desktop firefox nano install gedit nautilus xdg-utils xdg-user-dirs file-roller gnome-terminal-nautilus evince totem tilix tilix-nautilus file-roller-nautilus gnome-keyring gnome-photos gnome-system-monitor gnome-clocks gnome-disk-utility gnome-calculator gnome-backgrounds gnome-backgrounds-extras gstreamer1-plugins-{bad-\*,good-\*,base} gstreamer1-plugin-openh264 gstreamer1-libav --exclude=gstreamer1-plugins-bad-free-devel lame\* --exclude=lame-devel snapd VirtualBox vagrant git nodejs hplip system-config-printer-libs transmission-gtk php-cli -y
dnf group upgrade --with-optional Multimedia -y
ln -s /var/lib/snapd/snap /snap
systemctl reboot
